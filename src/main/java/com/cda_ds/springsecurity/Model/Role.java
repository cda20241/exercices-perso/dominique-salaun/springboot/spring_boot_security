package com.cda_ds.springsecurity.Model;

public enum Role {
    USER,
    ADMIN
}
