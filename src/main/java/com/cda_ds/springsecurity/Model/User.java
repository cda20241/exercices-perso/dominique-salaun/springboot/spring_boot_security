package com.cda_ds.springsecurity.Model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

// @Data génère les getters et les setters pour tous les champs(équivalent aux annotations @Getter et @Setter)
@Data
// aide à construire l'objet en utilisant le pattern Builder et génère ainsi automatiquement le constructeur de l'objet
@Builder
// indispensable à l'utilisation du @Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="utilisateur_id", nullable = false, updatable = false)
    private Long id;
    private String email;
    private String password;
    // ici on définit un rôle qui sera lié à notre méthode getAuthorities()
    @Enumerated(EnumType.STRING)
    private Role role;

    // renvoie une liste des rôles de l'utilisateur
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
